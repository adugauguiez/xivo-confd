XiVO confd
==========

[![Build Status](https://travis-ci.org/xivo-pbx/xivo-confd.png?branch=master)](https://travis-ci.org/xivo-pbx/xivo-confd)

XiVO CONFD is a web server that provides a [RESTful](http://en.wikipedia.org/wiki/Representational_state_transfer)
service for configuring and managing a XiVO server.


Installing xivo-confd
---------------------

The server is already provided as a part of [XiVO](http://documentation.xivo.solutions).
Please refer to [the documentation](https://documentation.xivo.solutions/en/stable/xivo/installation/installation.html) for
further details on installing one.


Running unit tests
------------------

```
apt-get install libpq-dev python-dev libffi-dev libyaml-dev
pip install tox
tox --recreate -e py27
```

Running integration tests
-------------------------

You need Docker installed and the package libpq-dev.

You need the repos xivo-db, xivo-provisioning and rabbitmq.

To clone them:

```
git clone https://gitlab.com/xivo.solutions/xivo-db.git
git clone https://gitlab.com/xivo.solutions/xivo-provisioning.git
git clone https://gitlab.com/xivo.solutions/rabbitmq.git
```

If you already have them, you can use them. In this case:

1. ensure they are on the right branch and up-to-date
2. edit ``integration_tests/Makefile``
3. update ``PROVD_DIR``, ``DB_DIR``, ``DB_VERSION`` and other for the ``make test-setup`` command

Alternatively you can clone them to the ``integration_tests`` directory
and then override the paths in the make command. E.g.: ``make test-setup DB_DIR=./xivo-db``

Run the tests:

```
cd integration_tests
pip install -U -r test-requirements.txt
make test-setup
make test
```

Run only selected tests:

```
nosetests suite/base/test_users.py
nosetests suite/base/test_users.py:test_create_user_with_all_parameters
```

Note: if you are modifying the dao, you will probably need to modify the requirements.txt of xivo-db project to point towards your working xivo-dao branch.


Development
-----------

### xivo-dao

In case you need to mount xivo_dao inside the xivo-confd container, add the
following line in confd volumes in
integration_tests/assets/base/docker-compose.yml

```
- "/path/to/xivo_dao:/usr/local/lib/python2.7/site-packages/xivo_dao"
```

### Modified database

If you need to run tests against a modified database schema, run:

```
make update-db DB_DIR=../../xivo-db DB_VERSION=2019.12.latest
```

### Quick multiple runs

If you need to run tests more than once (e.g. when developing):

```
make stop
make start
DOCKER=0 nosetests suite
```

You can also just run one test suite:
```
cd integration_tests
nosetests suite/base/test_users.py
```
