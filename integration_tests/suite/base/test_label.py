from __future__ import unicode_literals

from hamcrest import assert_that, contains_inanyorder, empty
from hamcrest import equal_to
from hamcrest import has_entries
from test_api import confd
from test_api import fixtures


@fixtures.label()
@fixtures.label()
def test_list_labels(first, second):
    response = confd.labels.get()

    assert_that(response.items, contains_inanyorder(has_entries(first), has_entries(second)))
    assert_that(response.total, equal_to(2))


@fixtures.label()
def test_delete_label(label):
    deleted = confd.labels(label['id']).delete()
    deleted.assert_deleted()

    label_list = confd.labels.get()
    assert_that(label_list.items, empty())


def test_add_labels():
    response = confd.labels.post(display_name='MyLabel')
    label_list = confd.labels.get()

    assert_that(response.item['display_name'], equal_to('MyLabel'))
    assert_that(label_list.total, equal_to(1))

    confd.labels(response.item['id']).delete()
