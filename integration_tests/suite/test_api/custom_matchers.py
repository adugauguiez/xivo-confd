from hamcrest.core.base_matcher import BaseMatcher
from hamcrest.core.helpers.hasmethod import hasmethod
from hamcrest import contains_inanyorder

class DictionnaryOfListMatcher(BaseMatcher):

    def __init__(self, entries):
        entries_type = type(entries)
        if entries_type is not dict:
            raise TypeError("Argument of invalid type, expected dict but got {}".format(entries_type))
        self.entries = entries

    def _matches(self, item):
        if type(item) is not dict:
            self.error = "Is not a dict" 
            return False

        for key, value in self.entries.iteritems():
            if not item.has_key(key):
                self.error = "{} was not found".format(key)
                return False
            if not contains_inanyorder(*value).matches(item[key]):
                self.error = "`{}` entry did not contain {} but {}".format(key, value, item[key])
                return False

        return True

    def describe_to(self, description):
        description.append_text(self.entries)    \
                   .append_text("\n\t")          \
                   .append_text(self.error)

def dictionnary_contains_inanyorder(entries):
    return DictionnaryOfListMatcher(entries)
