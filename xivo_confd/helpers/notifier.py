# -*- coding: UTF-8 -*-

# Copyright (C) 2018 Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..

import abc

from xivo_bus.resources.ipbx.event import ReloadIpbxEvent


class XdsNotifier(object):

    __metaclass__ = abc.ABCMeta

    def __init__(self, dao, bus):
        self.bus = bus
        self.dao = dao
        self.reload_commands = []

    def publish_reload_bus(self):
        self.publish_reload_bus_commands(self.reload_commands)

    def publish_reload_bus_commands(self, commands):
        if self.dao.is_live_reload_enabled():
            event = ReloadIpbxEvent(commands)
            self.bus.send_bus_event(event, event.routing_key)


class XdsResourceNotifier(XdsNotifier):

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def created(self, model):
        return

    @abc.abstractmethod
    def edited(self, model):
        return

    @abc.abstractmethod
    def deleted(self, model):
        return


class XdsAssociationNotifier(XdsNotifier):

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def associated(self, model):
        return

    @abc.abstractmethod
    def dissociated(self, model):
        return
