# -*- coding: utf-8 -*-

# Copyright (C) 2019 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


from xivo_confd import bus, sysconfd

from xivo_bus.resources.agent.event import (CreateAgentEvent,
                                            DeleteAgentEvent,
                                           EditAgentEvent)

class AgentNotifier(object):
    
    def __init__(self, _sysconfd, _bus):
        self.sysconfd = _sysconfd
        self.bus = _bus

    def send_sysconfd_handlers(self, action, agent_id):
        cti_command = 'xivo[agent,{},{}]'.format(action, agent_id)
        agent_bus = 'agent.{}.{}'.format(action, agent_id)
        handlers = {'ctibus': [cti_command, 'xivo[queuemember,update]'],
                    'agentbus': [agent_bus]}
        self.sysconfd.exec_request_handlers(handlers)

    def created(self, agent):
        self.send_sysconfd_handlers('add', agent.id)
        event = CreateAgentEvent(agent.id)
        self.bus.send_bus_event(event, event.routing_key)
        
    def edited(self, agent):
        self.send_sysconfd_handlers('edit', agent.id)
        event = EditAgentEvent(agent.id)
        self.bus.send_bus_event(event, event.routing_key)

    def deleted(self, agent):
        self.send_sysconfd_handlers('delete', agent.id)
        event = DeleteAgentEvent(agent.id)
        self.bus.send_bus_event(event, event.routing_key)

def build_notifier():
    return AgentNotifier(sysconfd, bus)
