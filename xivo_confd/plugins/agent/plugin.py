# -*- coding: utf-8 -*-

# Copyright (C) 2019 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from xivo_confd import api
from xivo_confd.plugins.agent.service import AgentService
from xivo_confd.plugins.agent.validator import build_validator
from xivo_confd.plugins.agent.notifier import build_notifier
from xivo_dao.resources.agent import dao as agent_dao
from xivo_confd.plugins.agent.resource import AgentItem, AgentList, AgentQueueList

import logging

logger = logging.getLogger(__name__)

class Plugin(object):

    def load(self, core):
        logger.warn("Loading agent plugin")
        provd_client = core.provd_client()
        service = AgentService(agent_dao,
                       build_validator(),
                       build_notifier())

        api.add_resource(AgentItem,
                         '/agents/<int:id>',
                         endpoint='agents',
                         resource_class_args=(service,)
                         )

        api.add_resource(AgentList,
                         '/agents',
                         endpoint='agents_list',
                         resource_class_args=(service,)
                         )

        api.add_resource(AgentQueueList,
                         '/agents/<int:id>/queues',
                         resource_class_args=(service,)
                         )
