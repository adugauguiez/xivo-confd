# -*- coding: UTF-8 -*-

# Copyright (C) 2019 Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..

import unittest
from hamcrest import assert_that, equal_to
from mock import Mock

from xivo_dao.alchemy.agentfeatures import AgentFeatures
from xivo_confd.plugins.agent.resource import AgentItem, AgentList, AgentQueueList

class TestAgentItem(unittest.TestCase):
    def setUp(self):
        self.service = Mock()
        self.agent_item = AgentItem(self.service)
        self.agent_item.parser = Mock()
        self.agent_item.persistor = Mock()
        
    def test_get_agent(self):
        af = AgentFeatures(id=1, firstname='James', lastname='Bond', number='007')
        self.service.get.return_value = af

        res = self.agent_item.get(1)
        assert_that(res['id'], equal_to(1))
        assert_that(res['firstname'], equal_to('James'))
        assert_that(res['lastname'], equal_to('Bond'))
        assert_that(res['number'], equal_to('007'))

    def test_put_agent(self):
        af = AgentFeatures(id=1, firstname='James', lastname='Bond', number='007')
        form = {
            'firstname': 'Secret',
            'lastname': 'Agent',
            'passwd': 'some'
        }
        self.service.get.return_value = af
        self.agent_item.parser.parse_args.return_value = form
        body, status = self.agent_item.put(1)
        
        self.service.edit.assert_called_with(af)
            
        assert_that(body, equal_to(''))
        assert_that(status, equal_to(204))


    def test_delete_agent(self):
        af = AgentFeatures(id=1, firstname='James', lastname='Bond', number='007')
        self.service.get.return_value = af

        body, status = self.agent_item.delete(1)

        self.agent_item.persistor.delete_agent_destination.assert_called_with(af)
        self.service.delete.assert_called_with(af)
        assert_that(body, equal_to(''))
        assert_that(status, equal_to(204))

class TestAgentQueueList(unittest.TestCase):
    def setUp(self):
        self.service = Mock()
        self.resource = AgentQueueList(self.service)


    def test_get_membership(self):
        memberships = [ {'agent_id': 123, 'queue_id': 1, 'penalty': 0},
                        {'agent_id': 123, 'queue_id': 2, 'penalty': 0}]
        
        self.service.get_agent_membership.return_value = memberships

        res = self.resource.get(123)
        assert_that(res, equal_to(memberships))
