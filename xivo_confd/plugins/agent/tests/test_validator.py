# -*- coding: UTF-8 -*-

# Copyright (C) 2015 Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import unittest
from mock import Mock

from xivo_dao.alchemy.agentgroup import AgentGroup
from xivo_dao.alchemy.agentfeatures import AgentFeatures
from xivo_dao.helpers.exception import NotFoundError

from xivo_confd.plugins.agent.validator import NumGroupExist


class TestNumGroupExist(unittest.TestCase):

    def setUp(self):
        self.dao = Mock()
        self.validator = NumGroupExist(self.dao)

    def test_given_group_exist_then_validation_passes(self):
        self.dao.get_group_by_id.return_value = AgentGroup(id=1)
        agent = AgentFeatures(numgroup=1)

        self.validator.validate(agent)

        self.dao.get_group_by_id.assert_called_once_with(1)

    def test_given_group_does_not_exist_then_raises_error(self):
        self.dao.get_group_by_id.return_value = None
        agent = AgentFeatures(numgroup=2)

        with self.assertRaises(NotFoundError):
            self.validator.validate(agent)

