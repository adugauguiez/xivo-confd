# -*- coding: utf-8 -*-

# Copyright (C) 2019 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
from xivo_confd.helpers.validator import (LANGUAGE_REGEX,
                                          Optional,
                                          RegexField,
                                          RequiredFields,
                                          FindResource,
                                          ResourceExists,
                                          ValidationGroup,
                                          Validator)

from xivo_dao.resources.context import dao as context_dao
from xivo_dao.resources.agent import dao as agent_dao
from xivo_dao.helpers import errors

PASSWD_REGEX = r"^[0-9\*#]*$"

class NumGroupExist(Validator):

    def __init__(self, dao):
        self.dao = dao

    def validate(self, agent):
        group = self.dao.get_group_by_id(agent.numgroup)
        if group is None:
            raise errors.not_found('AgentGroup', numgroup=agent.numgroup)



def build_validator():
    return ValidationGroup(
        common=[
            RequiredFields('firstname'),
            RequiredFields('lastname'),
            RequiredFields('number'),
            RequiredFields('numgroup'),
            NumGroupExist(agent_dao),
            RequiredFields('context'),
            FindResource('context', context_dao.find, 'Context'),
            Optional('passwd',
                     RegexField.compile('passwd', PASSWD_REGEX)),
            Optional('language',
                     RegexField.compile('language', LANGUAGE_REGEX)),
        ]
    )
