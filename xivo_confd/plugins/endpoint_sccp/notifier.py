# -*- coding: UTF-8 -*-

# Copyright (C) 2015-2016 Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from xivo_confd import bus
from xivo_confd.helpers.notifier import XdsResourceNotifier

from xivo_bus.resources.endpoint_sccp.event import CreateSccpEndpointEvent, \
    EditSccpEndpointEvent, DeleteSccpEndpointEvent

from xivo_dao.resources.configuration import dao


class SccpEndpointNotifier(XdsResourceNotifier):

    def __init__(self, bus, dao):
        self.bus = bus
        self.dao = dao
        self.reload_commands = ['module reload chan_sccp.so',
                                'dialplan reload']

    def created(self, line):
        event = CreateSccpEndpointEvent(line.id)
        self.bus.send_bus_event(event, event.routing_key)

    def edited(self, line):
        self.publish_reload_bus()
        event = EditSccpEndpointEvent(line.id)
        self.bus.send_bus_event(event, event.routing_key)

    def deleted(self, line):
        self.publish_reload_bus()
        event = DeleteSccpEndpointEvent(line.id)
        self.bus.send_bus_event(event, event.routing_key)


def build_notifier():
    return SccpEndpointNotifier(bus, dao)
