# -*- coding: UTF-8 -*-

# Copyright (C) 2015-2016 Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import unittest
from mock import Mock, call

from xivo_bus.resources.endpoint_sccp.event import CreateSccpEndpointEvent, \
    EditSccpEndpointEvent, DeleteSccpEndpointEvent
from xivo_bus.resources.ipbx.event import ReloadIpbxEvent

from xivo_confd.plugins.endpoint_sccp.notifier import SccpEndpointNotifier

from xivo_dao.alchemy.sccpline import SCCPLine as SCCPEndpoint


class TestSccpEndpointNotifier(unittest.TestCase):

    def setUp(self):
        self.bus = Mock()
        self.dao = Mock()
        self.sccp_endpoint = Mock(SCCPEndpoint, id=1234)
        self.reload_commands = ['module reload chan_sccp.so',
                                'dialplan reload']
        self.expected_reload_event = ReloadIpbxEvent(self.reload_commands)

        self.notifier = SccpEndpointNotifier(self.bus, self.dao)

    def reload_and_resource_call(self, resource_event):
        return [call(self.expected_reload_event, self.expected_reload_event.routing_key),
                call(resource_event, resource_event.routing_key)]

    def test_when_sccp_endpoint_created_then_sccp_not_reloaded(self):
        self.dao.is_live_reload_enabled.return_value = True
        expected_event = CreateSccpEndpointEvent(self.sccp_endpoint.id)

        self.notifier.created(self.sccp_endpoint)

        self.bus.send_bus_event.assert_called_once_with(expected_event,
                                                        expected_event.routing_key)

    def test_when_sccp_endpoint_edited_and_live_reload_enabled_then_sccp_reloaded(self):
        self.dao.is_live_reload_enabled.return_value = True
        expected_resource_event = EditSccpEndpointEvent(self.sccp_endpoint.id)
        expected_calls = self.reload_and_resource_call(expected_resource_event)

        self.notifier.edited(self.sccp_endpoint)

        self.bus.send_bus_event.assert_has_calls(expected_calls)

    def test_when_sccp_endpoint_edited_and_live_reload_disabled_then_sccp_not_reloaded(self):
        self.dao.is_live_reload_enabled.return_value = False
        expected_event = EditSccpEndpointEvent(self.sccp_endpoint.id)

        self.notifier.edited(self.sccp_endpoint)

        self.bus.send_bus_event.assert_called_once_with(expected_event,
                                                        expected_event.routing_key)

    def test_when_sccp_endpoint_deleted_and_live_reload_enabled_then_sccp_reloaded(self):
        self.dao.is_live_reload_enabled.return_value = True
        expected_resource_event = DeleteSccpEndpointEvent(self.sccp_endpoint.id)
        expected_calls = self.reload_and_resource_call(expected_resource_event)

        self.notifier.deleted(self.sccp_endpoint)

        self.bus.send_bus_event.assert_has_calls(expected_calls)

    def test_when_sccp_endpoint_deleted_and_live_reload_disabled_then_sccp_not_reloaded(self):
        self.dao.is_live_reload_enabled.return_value = False
        expected_event = DeleteSccpEndpointEvent(self.sccp_endpoint.id)

        self.notifier.deleted(self.sccp_endpoint)

        self.bus.send_bus_event.assert_called_once_with(expected_event,
                                                        expected_event.routing_key)
