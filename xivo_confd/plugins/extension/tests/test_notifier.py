# -*- coding: UTF-8 -*-

# Copyright (C) 2016 Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import unittest
from mock import Mock, call

from xivo_bus.resources.extension.event import (CreateExtensionEvent,
                                                EditExtensionEvent,
                                                DeleteExtensionEvent)
from xivo_bus.resources.ipbx.event import ReloadIpbxEvent

from xivo_confd.plugins.extension.notifier import ExtensionNotifier

from xivo_dao.alchemy.extension import Extension


class TestExtensionNotifier(unittest.TestCase):

    def setUp(self):
        self.bus = Mock()
        self.dao = Mock()
        self.extension = Mock(Extension, id=1234, exten='1000', context='default')

        self.notifier = ExtensionNotifier(self.bus, self.dao)

    def test_when_extension_created_and_live_reload_enabled_then_events_sent_on_bus(self):
        self.dao.is_live_reload_enabled.return_value = True
        expected_reload_commands = ['dialplan reload']
        expected_reload_event = ReloadIpbxEvent(expected_reload_commands)

        expected_resource_event = CreateExtensionEvent(self.extension.id,
                                                       self.extension.exten,
                                                       self.extension.context)
        expected_calls = [call(expected_reload_event, expected_reload_event.routing_key),
                          call(expected_resource_event, expected_resource_event.routing_key)]

        self.notifier.created(self.extension)

        self.bus.send_bus_event.assert_has_calls(expected_calls)

    def test_when_extension_created_and_live_reload_disabled_then_events_sent_on_bus(self):
        self.dao.is_live_reload_enabled.return_value = False
        expected_resource_event = CreateExtensionEvent(self.extension.id,
                                                       self.extension.exten,
                                                       self.extension.context)

        self.notifier.created(self.extension)

        self.bus.send_bus_event.assert_called_once_with(expected_resource_event, expected_resource_event.routing_key)

    def test_when_extension_edited_and_live_reload_enabled_then_event_sent_on_bus(self):
        self.dao.is_live_reload_enabled.return_value = True
        expected_reload_commands = ['dialplan reload',
                                    'sip reload',
                                    'module reload chan_sccp.so']
        expected_reload_event = ReloadIpbxEvent(expected_reload_commands)
        expected_resource_event = EditExtensionEvent(self.extension.id,
                                                     self.extension.exten,
                                                     self.extension.context)
        expected_calls = [call(expected_reload_event, expected_reload_event.routing_key),
                          call(expected_resource_event, expected_resource_event.routing_key)]

        self.notifier.edited(self.extension)

        self.bus.send_bus_event.assert_has_calls(expected_calls)

    def test_when_extension_edited_and_live_reload_disabled_then_events_sent_on_bus(self):
        self.dao.is_live_reload_enabled.return_value = False
        expected_resource_event = EditExtensionEvent(self.extension.id,
                                                     self.extension.exten,
                                                     self.extension.context)

        self.notifier.edited(self.extension)

        self.bus.send_bus_event.assert_called_once_with(expected_resource_event, expected_resource_event.routing_key)

    def test_when_extension_deleted_and_live_reload_enabled_then_event_sent_on_bus(self):
        self.dao.is_live_reload_enabled.return_value = True
        expected_reload_commands = ['dialplan reload']
        expected_reload_event = ReloadIpbxEvent(expected_reload_commands)
        expected_resource_event = DeleteExtensionEvent(self.extension.id,
                                                       self.extension.exten,
                                                       self.extension.context)
        expected_calls = [call(expected_reload_event, expected_reload_event.routing_key),
                          call(expected_resource_event, expected_resource_event.routing_key)]

        self.notifier.deleted(self.extension)

        self.bus.send_bus_event.assert_has_calls(expected_calls)

    def test_when_extension_deleted_and_live_reload_disabled_then_events_sent_on_bus(self):
        self.dao.is_live_reload_enabled.return_value = False
        expected_resource_event = DeleteExtensionEvent(self.extension.id,
                                                       self.extension.exten,
                                                       self.extension.context)

        self.notifier.deleted(self.extension)

        self.bus.send_bus_event.assert_called_once_with(expected_resource_event, expected_resource_event.routing_key)
