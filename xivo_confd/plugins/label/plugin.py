# -*- coding: UTF-8 -*-

# Copyright (C) 2020 Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import logging

from xivo_dao.resources.labels import dao as labels_dao
from xivo_dao.resources.labels import validator as dao_labels_validator

from xivo_confd import api
from xivo_confd.plugins.label.notifier import build_notifier
from xivo_confd.plugins.label.resource import LabelList, LabelItem
from xivo_confd.plugins.label.service import LabelService
from xivo_confd.plugins.label.validator import build_validator

logger = logging.getLogger(__name__)


class Plugin(object):

    def load(self, _):
        service = LabelService(labels_dao,
                               build_validator(dao_labels_validator),
                               build_notifier())

        api.add_resource(LabelItem,
                         '/labels/<int:id>',
                         resource_class_args=(service,)
                         )

        api.add_resource(LabelList,
                         '/labels',
                         resource_class_args=(service,)
                         )
