# -*- coding: UTF-8 -*-

# Copyright (C) 2020 Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..

import unittest

from hamcrest import assert_that, equal_to
from mock import Mock
from nose.tools import assert_raises
from xivo_dao.alchemy.labels import Labels
from xivo_dao.helpers.exception import NotFoundError

from xivo_confd.plugins.label.resource import LabelList, LabelItem


class TestLabelItem(unittest.TestCase):
    def setUp(self):
        self.service = Mock()
        self.labels = LabelItem(self.service)
        self.labels.parser = Mock()
        self.labels.persistor = Mock()

    def test_delete(self):
        lb = [Labels(id=1, display_name='Label One', description="this is a label")]
        self.service.get.return_value = lb

        body, status = self.labels.delete(1)

        self.service.delete.assert_called_with(lb)
        assert_that(body, equal_to(''))
        assert_that(status, equal_to(204))

    def test_delete_nonexistent_label(self):
        self.service.get.return_value = None

        assert_raises(NotFoundError, self.labels.delete, 1)


class TestLabelList(unittest.TestCase):
    def setUp(self):
        self.service = Mock()
        self.labels = LabelList(self.service)
        self.labels.parser = Mock()
        self.labels.persistor = Mock()
        self.labels.model = Mock()

    def test_get(self):
        lb = [Labels(id=1, display_name='Label One', description="this is a label")]
        self.service.search.return_value = (1, lb)

        res = self.labels.get()

        assert_that(res['items'][0]['id'], equal_to(1))
        assert_that(res['items'][0]['display_name'], equal_to('Label One'))
        assert_that(res['items'][0]['description'], equal_to('this is a label'))
        assert_that(res['total'], equal_to(1))

    def test_post(self):
        l = Labels(id=1, display_name='MyLabel', description="this is a label")
        self.service.create.return_value = l

        form = {
            'display_name': 'MyLabel',
            'description': 'this is a label'
        }

        self.labels.parser.parse_args.return_value = form
        res = self.labels.post()

        assert_that(res[0]['id'], equal_to(1))
        assert_that(res[0]['display_name'], equal_to('MyLabel'))
        assert_that(res[0]['description'], equal_to('this is a label'))
        assert_that(res[0]['users_count'], equal_to(0))
