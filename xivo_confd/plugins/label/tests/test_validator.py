# -*- coding: UTF-8 -*-

# Copyright (C) 2020 Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..

import unittest

from mock import Mock
from xivo_dao.alchemy.labels import Labels
from xivo_dao.helpers.exception import ResourceError

from xivo_confd.plugins.label.validator import build_validator


class TestLabelList(unittest.TestCase):
    def setUp(self):
        self.dao_validator = Mock()
        self.validator = build_validator(self.dao_validator)

    def test_given_label_with_same_display_name_exists_then_validation_fails(self):
        label = Mock(Labels, display_name='label1')
        self.dao_validator.validate_create.return_value = label

        self.assertRaises(ResourceError, self.validator.validate_create, label)
        self.dao_validator.validate_create.assert_called_once_with('label1')
