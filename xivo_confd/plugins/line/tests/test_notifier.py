# -*- coding: UTF-8 -*-

# Copyright (C) 2015-2016 Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import unittest
from mock import Mock, call

from xivo_bus.resources.line.event import CreateLineEvent, \
    EditLineEvent, DeleteLineEvent
from xivo_bus.resources.ipbx.event import ReloadIpbxEvent

from xivo_confd.plugins.line.notifier import LineNotifier

from xivo_dao.alchemy.linefeatures import LineFeatures as Line


class TestLineNotifier(unittest.TestCase):

    def setUp(self):
        self.bus = Mock()
        self.dao = Mock()
        self.line = Mock(Line, id=1234)
        self.reload_commands = ['sip reload',
                                'dialplan reload',
                                'module reload chan_sccp.so']
        self.expected_reload_event = ReloadIpbxEvent(self.reload_commands)

        self.notifier = LineNotifier(self.bus, self.dao)

    def reload_and_resource_call(self, resource_event):
        return [call(self.expected_reload_event, self.expected_reload_event.routing_key),
                call(resource_event, resource_event.routing_key)]

    def test_when_line_created_and_live_reload_enabled_then_events_sent_on_bus(self):
        self.dao.is_live_reload_enabled.return_value = True
        expected_resource_event = CreateLineEvent(self.line.id)
        expected_calls = self.reload_and_resource_call(expected_resource_event)

        self.notifier.created(self.line)

        self.bus.send_bus_event.assert_has_calls(expected_calls)

    def test_when_line_created_and_live_reload_disabled_then_events_sent_on_bus(self):
        self.dao.is_live_reload_enabled.return_value = False
        expected_resource_event = CreateLineEvent(self.line.id)

        self.notifier.created(self.line)

        self.bus.send_bus_event.assert_called_once_with(expected_resource_event, expected_resource_event.routing_key)

    def test_when_line_edited_and_live_reload_enabled_then_events_sent_on_bus(self):
        self.dao.is_live_reload_enabled.return_value = True
        expected_resource_event = EditLineEvent(self.line.id)
        expected_calls = self.reload_and_resource_call(expected_resource_event)

        self.notifier.edited(self.line)

        self.bus.send_bus_event.assert_has_calls(expected_calls)

    def test_when_line_edited_and_live_reload_disabled_then_events_sent_on_bus(self):
        self.dao.is_live_reload_enabled.return_value = False
        expected_resource_event = EditLineEvent(self.line.id)

        self.notifier.edited(self.line)

        self.bus.send_bus_event.assert_called_once_with(expected_resource_event, expected_resource_event.routing_key)

    def test_when_line_deleted_and_live_reload_enabled_then_events_sent_on_bus(self):
        self.dao.is_live_reload_enabled.return_value = True
        expected_resource_event = DeleteLineEvent(self.line.id)
        expected_calls = self.reload_and_resource_call(expected_resource_event)

        self.notifier.deleted(self.line)

        self.bus.send_bus_event.assert_has_calls(expected_calls)

    def test_when_line_deleted_and_live_reload_disabled_then_events_sent_on_bus(self):
        self.dao.is_live_reload_enabled.return_value = False
        expected_resource_event = DeleteLineEvent(self.line.id)

        self.notifier.deleted(self.line)

        self.bus.send_bus_event.assert_called_once_with(expected_resource_event, expected_resource_event.routing_key)
