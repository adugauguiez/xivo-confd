# -*- coding: UTF-8 -*-

# Copyright (C) 2016 Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from xivo_confd import bus
from xivo_confd.helpers.notifier import XdsAssociationNotifier

from xivo_dao.resources.configuration import dao


class LineDeviceNotifier(XdsAssociationNotifier):

    def __init__(self, bus, dao):
        self.bus = bus
        self.dao = dao
        self.reload_commands = ['module reload chan_sccp.so']

    def associated(self, line, device):
        self.reload_sccp(line)

    def dissociated(self, line, device):
        self.reload_sccp(line)

    def reload_sccp(self, line):
        if line.endpoint == "sccp":
            self.publish_reload_bus()


def build_notifier():
    return LineDeviceNotifier(bus, dao)
