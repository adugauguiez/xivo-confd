# -*- coding: UTF-8 -*-

# Copyright (C) 2016 Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import unittest

from mock import Mock

from hamcrest import assert_that, equal_to

from xivo_bus.resources.ipbx.event import ReloadIpbxEvent

from xivo_dao.alchemy.linefeatures import LineFeatures as Line
from xivo_confd.plugins.device.model import Device

from xivo_confd.plugins.line_device.notifier import LineDeviceNotifier


class TestLineDeviceNotifier(unittest.TestCase):

    def setUp(self):
        self.bus = Mock()
        self.dao = Mock()
        self.line = Mock(Line)
        self.device = Mock(Device)
        self.reload_commands = ['module reload chan_sccp.so']
        self.expected_reload_event = ReloadIpbxEvent(self.reload_commands)

        self.notifier = LineDeviceNotifier(self.bus, self.dao)

    def test_given_line_is_not_sccp_when_associated_then_sccp_not_reloaded(self):
        self.dao.is_live_reload_enabled.return_value = True
        self.line.endpoint = "sip"

        self.notifier.associated(self.line, self.device)

        assert_that(self.bus.send_bus_event.call_count, equal_to(0))

    def test_given_line_is_not_sccp_when_dissociated_then_sccp_not_reloaded(self):
        self.dao.is_live_reload_enabled.return_value = True
        self.line.endpoint = "sip"

        self.notifier.dissociated(self.line, self.device)

        assert_that(self.bus.send_bus_event.call_count, equal_to(0))

    def test_given_line_is_sccp_when_associated_and_live_reload_enabled_then_sccp_reloaded(self):
        self.dao.is_live_reload_enabled.return_value = True
        self.line.endpoint = "sccp"

        self.notifier.associated(self.line, self.device)

        self.bus.send_bus_event.assert_called_once_with(self.expected_reload_event, self.expected_reload_event.routing_key)

    def test_given_line_is_sccp_when_associated_and_live_reload_disabled_then_sccp_not_reloaded(self):
        self.dao.is_live_reload_enabled.return_value = False
        self.line.endpoint = "sccp"

        self.notifier.associated(self.line, self.device)

        self.assertEquals(self.bus.send_bus_event.call_count, 0)

    def test_given_line_is_sccp_when_dissociated_and_live_reload_enabled_then_sccp_reloaded(self):
        self.dao.is_live_reload_enabled.return_value = True
        self.line.endpoint = "sccp"

        self.notifier.dissociated(self.line, self.device)

        self.bus.send_bus_event.assert_called_once_with(self.expected_reload_event, self.expected_reload_event.routing_key)

    def test_given_line_is_sccp_when_dissociated_and_live_reload_disabled_then_sccp_not_reloaded(self):
        self.dao.is_live_reload_enabled.return_value = False
        self.line.endpoint = "sccp"

        self.notifier.dissociated(self.line, self.device)

        self.assertEquals(self.bus.send_bus_event.call_count, 0)
