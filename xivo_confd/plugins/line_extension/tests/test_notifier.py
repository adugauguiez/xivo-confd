# -*- coding: utf-8 -*-

# Copyright (C) 2013-2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import unittest
from mock import patch, Mock, call

from xivo_bus.resources.ipbx.event import ReloadIpbxEvent

from xivo_confd.plugins.line_extension.manager import LineExtension
from xivo_confd.plugins.line_extension.notifier import LineExtensionNotifier

from xivo_dao.resources.user_line.model import UserLine


class TestLineExtensionNotifier(unittest.TestCase):

    def setUp(self):
        self.sysconf = Mock()
        self.bus = Mock()
        self.dao = Mock()

        self.notifier = LineExtensionNotifier(self.sysconf, self.bus, self.dao)

    def reload_and_association_call(self, association_event):
        reload_commands = ['dialplan reload',
                           'sip reload']
        expected_reload_event = ReloadIpbxEvent(reload_commands)

        return [call(expected_reload_event, expected_reload_event.routing_key),
                call(association_event, association_event.routing_key)]

    def test_associated(self):
        self.notifier.send_sysconf_commands = Mock()
        self.notifier.send_bus_association_events = Mock()
        line_extension = LineExtension(line_id=1, extension_id=2)

        self.notifier.associated(line_extension)

        self.notifier.send_sysconf_commands.assert_called_once_with(line_extension)
        self.notifier.send_bus_association_events.assert_called_once_with(line_extension)

    @patch('xivo_confd.helpers.sysconfd_connector.exec_request_handlers')
    @patch('xivo_dao.resources.user_line.dao.find_all_by_line_id')
    def test_send_sysconf_commands(self,
                                   find_all_by_line_id,
                                   exec_request_handlers):
        line_extension = LineExtension(line_id=1, extension_id=2)
        user_line_1 = Mock(UserLine, line_id=1, user_id=3)
        user_line_2 = Mock(UserLine, line_id=1, user_id=None)

        find_all_by_line_id.return_value = [user_line_1, user_line_2]

        expected_sysconf_command = {
            'ctibus': ['xivo[phone,edit,1]', 'xivo[user,edit,3]'],
            'agentbus': []
        }

        self.notifier.send_sysconf_commands(line_extension)

        exec_request_handlers.assert_called_once_with(expected_sysconf_command)
        find_all_by_line_id.assert_called_once_with(line_extension.line_id)

    @patch('xivo_bus.resources.line_extension.event.LineExtensionAssociatedEvent')
    def test_send_bus_association_events_when_live_reload_enabled(self, LineExtensionAssociatedEvent):
        self.dao.is_live_reload_enabled.return_value = True
        expected_association_event = LineExtensionAssociatedEvent.return_value = Mock()
        expected_calls = self.reload_and_association_call(expected_association_event)

        line_extension = LineExtension(line_id=1, extension_id=2)

        self.notifier.send_bus_association_events(line_extension)

        LineExtensionAssociatedEvent.assert_called_once_with(line_extension.line_id,
                                                             line_extension.extension_id)
        self.bus.send_bus_event.assert_has_calls(expected_calls)

    @patch('xivo_bus.resources.line_extension.event.LineExtensionAssociatedEvent')
    def test_send_bus_association_events_when_live_reload_disabled(self, LineExtensionAssociatedEvent):
        self.dao.is_live_reload_enabled.return_value = False
        expected_event = LineExtensionAssociatedEvent.return_value = Mock()

        line_extension = LineExtension(line_id=1, extension_id=2)

        self.notifier.send_bus_association_events(line_extension)

        LineExtensionAssociatedEvent.assert_called_once_with(line_extension.line_id,
                                                             line_extension.extension_id)
        self.bus.send_bus_event.assert_called_once_with(expected_event, expected_event.routing_key)

    def test_dissociated(self):
        self.notifier.send_sysconf_commands = Mock()
        self.notifier.send_bus_dissociation_events = Mock()
        line_extension = LineExtension(line_id=1, extension_id=2)

        self.notifier.dissociated(line_extension)

        self.notifier.send_sysconf_commands.assert_called_once_with(line_extension)
        self.notifier.send_bus_dissociation_events.assert_called_once_with(line_extension)

    @patch('xivo_bus.resources.line_extension.event.LineExtensionDissociatedEvent')
    def test_send_bus_dissociation_events_when_live_reload_enabled(self, LineExtensionDissociatedEvent):
        self.dao.is_live_reload_enabled.return_value = True
        expected_dissociation_event = LineExtensionDissociatedEvent.return_value = Mock()
        expected_calls = self.reload_and_association_call(expected_dissociation_event)

        line_extension = LineExtension(line_id=1, extension_id=2)

        self.notifier.send_bus_dissociation_events(line_extension)

        LineExtensionDissociatedEvent.assert_called_once_with(line_extension.line_id,
                                                              line_extension.extension_id)
        self.bus.send_bus_event.assert_has_calls(expected_calls)

    @patch('xivo_bus.resources.line_extension.event.LineExtensionDissociatedEvent')
    def test_send_bus_dissociation_events_when_live_reload_disabled(self, LineExtensionDissociatedEvent):
        self.dao.is_live_reload_enabled.return_value = False
        expected_event = LineExtensionDissociatedEvent.return_value = Mock()

        line_extension = LineExtension(line_id=1, extension_id=2)

        self.notifier.send_bus_dissociation_events(line_extension)

        LineExtensionDissociatedEvent.assert_called_once_with(line_extension.line_id,
                                                              line_extension.extension_id)
        self.bus.send_bus_event.assert_called_once_with(expected_event, expected_event.routing_key)
