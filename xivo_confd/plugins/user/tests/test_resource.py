# -*- coding: UTF-8 -*-

# Copyright (C) 2018 Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..

import unittest
from hamcrest import assert_that, equal_to
from mock import Mock

from collections import namedtuple
from xivo_dao.alchemy.userfeatures import UserFeatures

from xivo_confd.plugins.user.resource import UserItem

UserfeaturesAndAgentnumber = namedtuple('UserfeaturesAndAgentnumber', ['UserFeatures', 'agent_number', 'agent_group_id'])

class TestUserItem(unittest.TestCase):

    def setUp(self):
        self.service = Mock()
        self.user_item = UserItem(self.service)

    def test_get_returns_user_with_agentnumber(self):
        uf = UserFeatures(firstname='foo')
        ufa = UserfeaturesAndAgentnumber(uf, '8000', 1)
        self.service.get_with_agent.return_value = ufa

        res = self.user_item.get(1)

        assert_that(res['firstname'], equal_to('foo'))
        assert_that(res['agent_number'], equal_to('8000'))
        assert_that(res['agent_group_id'], equal_to(1))
        assert_that(res['mobile_phone_number'], equal_to(None))

    def test_get_returns_user_without_agentnumber(self):
        uf = UserFeatures(firstname='foo')
        ufa = UserfeaturesAndAgentnumber(uf, None, None)
        self.service.get_with_agent.return_value = ufa

        res = self.user_item.get(1)

        assert_that(res['firstname'], equal_to('foo'))
        assert_that(res['agent_number'], equal_to(None))
        assert_that(res['agent_group_id'], equal_to(None))
        assert_that(res['mobile_phone_number'], equal_to(None))

    def test_get_returns_user_with_other_fields(self):
        uf = UserFeatures(firstname='foo',
                          mobilephonenumber='0654321098')
        ufa = UserfeaturesAndAgentnumber(uf, '8000', 12)
        self.service.get_with_agent.return_value = ufa

        res = self.user_item.get(1)

        assert_that(res['firstname'], equal_to('foo'))
        assert_that(res['agent_number'], equal_to('8000'))
        assert_that(res['agent_group_id'], equal_to(12))
        assert_that(res['mobile_phone_number'], equal_to('0654321098'))
