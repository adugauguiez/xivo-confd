# -*- coding: utf-8 -*-

# Copyright (C) 2013-2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from xivo_bus.resources.user_line import event

from xivo_confd import bus
from xivo_confd.helpers.notifier import XdsAssociationNotifier

from xivo_dao.resources.configuration import dao


class UserLineNotifier(XdsAssociationNotifier):

    def __init__(self, bus, dao):
        self.bus = bus
        self.dao = dao
        self.reload_commands = ['dialplan reload',
                                'sip reload']

    def associated(self, user_line):
        self.bus_event_associated(user_line)

    def dissociated(self, user_line):
        self.bus_event_dissociated(user_line)

    def bus_event_associated(self, user_line):
        self.publish_reload_bus()
        bus_event = event.UserLineAssociatedEvent(user_line.user_id,
                                                  user_line.line_id,
                                                  user_line.main_user,
                                                  user_line.main_line)
        self.bus.send_bus_event(bus_event, bus_event.routing_key)

    def bus_event_dissociated(self, user_line):
        self.publish_reload_bus()
        bus_event = event.UserLineDissociatedEvent(user_line.user_id,
                                                   user_line.line_id,
                                                   user_line.main_user,
                                                   user_line.main_line)
        self.bus.send_bus_event(bus_event, bus_event.routing_key)


def build_notifier():
    return UserLineNotifier(bus, dao)
