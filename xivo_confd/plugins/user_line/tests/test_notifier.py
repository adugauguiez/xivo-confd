# -*- coding: utf-8 -*-

# Copyright (C) 2013-2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import unittest
from mock import patch, Mock, call

from xivo_bus.resources.ipbx.event import ReloadIpbxEvent

from xivo_dao.resources.user_line.model import UserLine

from xivo_confd.plugins.user_line.notifier import UserLineNotifier


class TestUserLineNotifier(unittest.TestCase):

    def setUp(self):
        self.bus = Mock()
        self.dao = Mock()

        self.notifier = UserLineNotifier(self.bus, self.dao)

    def reload_and_association_call(self, association_event):
        self.reload_commands = ['dialplan reload',
                                'sip reload']
        self.expected_reload_event = ReloadIpbxEvent(self.reload_commands)

        return [call(self.expected_reload_event, self.expected_reload_event.routing_key),
                call(association_event, association_event.routing_key)]

    def test_associated(self):
        self.notifier.bus_event_associated = Mock()
        user_line = UserLine(user_id=1, line_id=2)

        self.notifier.associated(user_line)

        self.notifier.bus_event_associated.assert_called_once_with(user_line)

    @patch('xivo_bus.resources.user_line.event.UserLineAssociatedEvent')
    def test_bus_event_associated_when_live_reload_enabled(self, UserLineAssociatedEvent):
        self.dao.is_live_reload_enabled.return_value = True
        expected_association_event = UserLineAssociatedEvent.return_value = Mock()
        expected_calls = self.reload_and_association_call(expected_association_event)
        user_line = UserLine(user_id=1, line_id=2)

        self.notifier.bus_event_associated(user_line)

        UserLineAssociatedEvent.assert_called_once_with(user_line.user_id,
                                                        user_line.line_id,
                                                        True,
                                                        True)
        self.bus.send_bus_event.assert_has_calls(expected_calls)

    @patch('xivo_bus.resources.user_line.event.UserLineAssociatedEvent')
    def test_bus_event_associated_when_live_reload_disabled(self, UserLineAssociatedEvent):
        self.dao.is_live_reload_enabled.return_value = False
        expected_association_event = UserLineAssociatedEvent.return_value = Mock()
        user_line = UserLine(user_id=1, line_id=2)

        self.notifier.bus_event_associated(user_line)

        UserLineAssociatedEvent.assert_called_once_with(user_line.user_id,
                                                        user_line.line_id,
                                                        True,
                                                        True)
        self.bus.send_bus_event.assert_called_once_with(expected_association_event, expected_association_event.routing_key)

    def test_dissociated(self):
        self.notifier.bus_event_dissociated = Mock()
        user_line = UserLine(user_id=1, line_id=2)

        self.notifier.dissociated(user_line)

        self.notifier.bus_event_dissociated.assert_called_once_with(user_line)

    @patch('xivo_bus.resources.user_line.event.UserLineDissociatedEvent')
    def test_bus_event_dissociated_when_live_reload_enabled(self, UserLineDissociatedEvent):
        self.dao.is_live_reload_enabled.return_value = True
        expected_dissociation_event = UserLineDissociatedEvent.return_value = Mock()
        expected_calls = self.reload_and_association_call(expected_dissociation_event)
        user_line = UserLine(user_id=1, line_id=2)

        self.notifier.bus_event_dissociated(user_line)

        UserLineDissociatedEvent.assert_called_once_with(user_line.user_id,
                                                         user_line.line_id,
                                                         True,
                                                         True)
        self.bus.send_bus_event.assert_has_calls(expected_calls)

    @patch('xivo_bus.resources.user_line.event.UserLineDissociatedEvent')
    def test_bus_event_dissociated_when_live_reload_disabled(self, UserLineDissociatedEvent):
        self.dao.is_live_reload_enabled.return_value = False
        expected_dissociation_event = UserLineDissociatedEvent.return_value = Mock()
        user_line = UserLine(user_id=1, line_id=2)

        self.notifier.bus_event_dissociated(user_line)

        UserLineDissociatedEvent.assert_called_once_with(user_line.user_id,
                                                         user_line.line_id,
                                                         True,
                                                         True)
        self.bus.send_bus_event.assert_called_once_with(expected_dissociation_event, expected_dissociation_event.routing_key)
