# -*- coding: utf-8 -*-

# Copyright (C) 2013-2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import unittest
from mock import patch, Mock, call

from xivo_bus.resources.ipbx.event import ReloadIpbxEvent
from xivo_dao.resources.user_line.model import UserLine
from xivo_dao.resources.user_voicemail.model import UserVoicemail

from xivo_confd.plugins.user_voicemail.notifier import UserVoicemailNotifier


class TestUserVoicemailNotifier(unittest.TestCase):

    def setUp(self):
        self.bus = Mock()
        self.dao = Mock()

        self.notifier = UserVoicemailNotifier(self.bus, self.dao)

    def reload_and_association_call(self, association_event):
        reload_commands = ['sip reload',
                           'module reload chan_sccp.so']
        expected_reload_event = ReloadIpbxEvent(reload_commands)

        return [call(expected_reload_event, expected_reload_event.routing_key),
                call(association_event, association_event.routing_key)]

    def test_associated(self):
        self.notifier.sysconf_command_association_updated = Mock()
        self.notifier.bus_event_associated = Mock()
        user_voicemail = UserVoicemail(user_id=1, voicemail_id=2)

        self.notifier.associated(user_voicemail)

        self.notifier.sysconf_command_association_updated.assert_called_once_with(user_voicemail)
        self.notifier.bus_event_associated.assert_called_once_with(user_voicemail)

    @patch('xivo_confd.helpers.sysconfd_connector.exec_request_handlers')
    @patch('xivo_dao.resources.user_line.dao.find_all_by_user_id')
    def test_send_sysconf_command_association_updated(self, find_all_by_user_id, exec_request_handlers):
        user_voicemail = UserVoicemail(user_id=1, voicemail_id=2)
        user_line_1 = Mock(UserLine, line_id=3)
        user_line_2 = Mock(UserLine, line_id=4)

        find_all_by_user_id.return_value = [user_line_1, user_line_2]

        expected_sysconf_command = {
            'ctibus': ['xivo[user,edit,1]', 'xivo[phone,edit,3]', 'xivo[phone,edit,4]'],
            'agentbus': []
        }

        self.notifier.sysconf_command_association_updated(user_voicemail)

        exec_request_handlers.assert_called_once_with(expected_sysconf_command)
        find_all_by_user_id.assert_called_once_with(user_voicemail.user_id)

    @patch('xivo_bus.resources.user_voicemail.event.UserVoicemailAssociatedEvent')
    def test_bus_event_dissociated_when_live_reload_enabled(self, UserVoicemailAssociatedEvent):
        self.dao.is_live_reload_enabled.return_value = True
        expected_association_event = UserVoicemailAssociatedEvent.return_value = Mock()
        expected_calls = self.reload_and_association_call(expected_association_event)

        user_voicemail = UserVoicemail(user_id=1, voicemail_id=2, enabled=True)

        self.notifier.bus_event_associated(user_voicemail)

        UserVoicemailAssociatedEvent.assert_called_once_with(user_voicemail.user_id,
                                                            user_voicemail.voicemail_id,
                                                            user_voicemail.enabled)
        self.bus.send_bus_event.assert_has_calls(expected_calls)

    @patch('xivo_bus.resources.user_voicemail.event.UserVoicemailAssociatedEvent')
    def test_bus_event_associated_live_reload_disabled(self, UserVoicemailAssociatedEvent):
        self.dao.is_live_reload_enabled.return_value = False
        new_event = UserVoicemailAssociatedEvent.return_value = Mock()

        user_voicemail = UserVoicemail(user_id=1, voicemail_id=2, enabled=True)

        self.notifier.bus_event_associated(user_voicemail)

        UserVoicemailAssociatedEvent.assert_called_once_with(user_voicemail.user_id,
                                                            user_voicemail.voicemail_id,
                                                            user_voicemail.enabled)
        self.bus.send_bus_event.assert_called_once_with(new_event, new_event.routing_key)

    def test_dissociated(self):
        self.notifier.sysconf_command_association_updated = Mock()
        self.notifier.bus_event_dissociated = Mock()
        user_voicemail = UserVoicemail(user_id=1, voicemail_id=2)

        self.notifier.dissociated(user_voicemail)

        self.notifier.sysconf_command_association_updated.assert_called_once_with(user_voicemail)
        self.notifier.bus_event_dissociated.assert_called_once_with(user_voicemail)

    @patch('xivo_bus.resources.user_voicemail.event.UserVoicemailDissociatedEvent')
    def test_bus_event_dissociated_when_live_reload_enabled(self, UserVoicemailDissociatedEvent):
        self.dao.is_live_reload_enabled.return_value = True
        expected_association_event = UserVoicemailDissociatedEvent.return_value = Mock()
        expected_calls = self.reload_and_association_call(expected_association_event)

        user_voicemail = UserVoicemail(user_id=1, voicemail_id=2)

        self.notifier.bus_event_dissociated(user_voicemail)

        UserVoicemailDissociatedEvent.assert_called_once_with(user_voicemail.user_id,
                                                             user_voicemail.voicemail_id,
                                                             False)
        self.bus.send_bus_event.assert_has_calls(expected_calls)

    @patch('xivo_bus.resources.user_voicemail.event.UserVoicemailDissociatedEvent')
    def test_bus_event_dissociated_when_live_reload_disabled(self, UserVoicemailDissociatedEvent):
        self.dao.is_live_reload_enabled.return_value = False
        new_event = UserVoicemailDissociatedEvent.return_value = Mock()

        user_voicemail = UserVoicemail(user_id=1, voicemail_id=2)

        self.notifier.bus_event_dissociated(user_voicemail)

        UserVoicemailDissociatedEvent.assert_called_once_with(user_voicemail.user_id,
                                                             user_voicemail.voicemail_id,
                                                             False)
        self.bus.send_bus_event.assert_called_once_with(new_event, new_event.routing_key)
