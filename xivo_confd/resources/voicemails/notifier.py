# -*- coding: utf-8 -*-

# Copyright (C) 2013-2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from xivo_confd import bus, sysconfd
from xivo_confd.helpers.notifier import XdsResourceNotifier

from xivo_bus.resources.voicemail.event import CreateVoicemailEvent
from xivo_bus.resources.voicemail.event import EditVoicemailEvent
from xivo_bus.resources.voicemail.event import DeleteVoicemailEvent

from xivo_dao.resources.configuration import dao


class VoicemailsNotifier(XdsResourceNotifier):

    def __init__(self, sysconfd, bus, dao):
        self.sysconfd = sysconfd
        self.bus = bus
        self.dao = dao

    def _new_sysconfd_data(self, ctibus_command):
        return {
            'ctibus': [ctibus_command],
            'agentbus': []
        }

    def created(self, voicemail):
        reload_commands = ['voicemail reload']
        data = self._new_sysconfd_data('xivo[voicemail,add,%s]' % voicemail.id)
        self.publish_reload_bus_commands(reload_commands)
        self.sysconfd.exec_request_handlers(data)
        event = CreateVoicemailEvent(voicemail.id)
        self.bus.send_bus_event(event, event.routing_key)

    def edited(self, voicemail):
        reload_commands = ['voicemail reload',
                           'sip reload',
                           'module reload chan_sccp.so']
        data = self._new_sysconfd_data('xivo[voicemail,edit,%s]' % voicemail.id)
        self.publish_reload_bus_commands(reload_commands)
        self.sysconfd.exec_request_handlers(data)
        event = EditVoicemailEvent(voicemail.id)
        self.bus.send_bus_event(event, event.routing_key)

    def deleted(self, voicemail):
        reload_commands = ['voicemail reload']
        data = self._new_sysconfd_data('xivo[voicemail,delete,%s]' % voicemail.id)
        self.publish_reload_bus_commands(reload_commands)
        self.sysconfd.exec_request_handlers(data)
        event = DeleteVoicemailEvent(voicemail.id)
        self.bus.send_bus_event(event, event.routing_key)


def build_notifier():
    return VoicemailsNotifier(sysconfd, bus, dao)
