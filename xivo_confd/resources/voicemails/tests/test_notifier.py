# -*- coding: utf-8 -*-

# Copyright (C) 2013-2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import unittest
from mock import Mock, call

from xivo_bus.resources.ipbx.event import ReloadIpbxEvent
from xivo_bus.resources.voicemail.event import CreateVoicemailEvent
from xivo_bus.resources.voicemail.event import EditVoicemailEvent
from xivo_bus.resources.voicemail.event import DeleteVoicemailEvent

from xivo_dao.resources.voicemail.model import Voicemail

from xivo_confd.resources.voicemails.notifier import VoicemailsNotifier


class TestVoicemailsNotifier(unittest.TestCase):

    def setUp(self):
        self.sysconf = Mock()
        self.bus = Mock()
        self.dao = Mock()
        self.validator = Mock()
        self.notifier = VoicemailsNotifier(self.sysconf, self.bus, self.dao)
        self.voicemail = Voicemail(id='123')

    def test_when_voicemail_created_and_live_reload_enabled_then_events_sent_on_bus(self):
        self.dao.is_live_reload_enabled.return_value = True
        expected_reload_commands = ['voicemail reload']
        expected_reload_event = ReloadIpbxEvent(expected_reload_commands)
        expected_resource_event = CreateVoicemailEvent(self.voicemail.id)
        expected_calls = [call(expected_reload_event, expected_reload_event.routing_key),
                          call(expected_resource_event, expected_resource_event.routing_key)]

        self.notifier.created(self.voicemail)

        self.bus.send_bus_event.assert_has_calls(expected_calls)

    def test_when_voicemail_created_and_live_reload_disabled_then_events_sent_on_bus(self):
        self.dao.is_live_reload_enabled.return_value = False
        expected_resource_event = CreateVoicemailEvent(self.voicemail.id)

        self.notifier.created(self.voicemail)

        self.bus.send_bus_event.assert_called_once_with(expected_resource_event, expected_resource_event.routing_key)

    def test_when_voicemail_edited_and_live_reload_enabled_then_events_sent_on_bus(self):
        self.dao.is_live_reload_enabled.return_value = True
        expected_reload_commands = ['voicemail reload',
                                    'sip reload',
                                    'module reload chan_sccp.so']
        expected_reload_event = ReloadIpbxEvent(expected_reload_commands)
        expected_resource_event = EditVoicemailEvent(self.voicemail.id)
        expected_calls = [call(expected_reload_event, expected_reload_event.routing_key),
                          call(expected_resource_event, expected_resource_event.routing_key)]

        self.notifier.edited(self.voicemail)

        self.bus.send_bus_event.assert_has_calls(expected_calls)

    def test_when_voicemail_edited_and_live_reload_disabled_then_events_sent_on_bus(self):
        self.dao.is_live_reload_enabled.return_value = False
        expected_resource_event = EditVoicemailEvent(self.voicemail.id)

        self.notifier.edited(self.voicemail)

        self.bus.send_bus_event.assert_called_once_with(expected_resource_event, expected_resource_event.routing_key)

    def test_when_voicemail_deleted_and_live_reload_enabled_then_events_sent_on_bus(self):
        self.dao.is_live_reload_enabled.return_value = True
        expected_reload_commands = ['voicemail reload']
        expected_reload_event = ReloadIpbxEvent(expected_reload_commands)
        expected_resource_event = DeleteVoicemailEvent(self.voicemail.id)
        expected_calls = [call(expected_reload_event, expected_reload_event.routing_key),
                          call(expected_resource_event, expected_resource_event.routing_key)]

        self.notifier.deleted(self.voicemail)

        self.bus.send_bus_event.assert_has_calls(expected_calls)

    def test_when_voicemail_deleted_and_live_reload_disabled_then_events_sent_on_bus(self):
        self.dao.is_live_reload_enabled.return_value = False
        expected_resource_event = DeleteVoicemailEvent(self.voicemail.id)

        self.notifier.deleted(self.voicemail)

        self.bus.send_bus_event.assert_called_once_with(expected_resource_event, expected_resource_event.routing_key)
